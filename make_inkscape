#!/bin/bash

# Copyright (c) 2017-2019 Maren Hachmann
#               2019 Thomas Holder
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# exit on error
set -e

# -------------------
# --- GLOBALS ---

# Determine the real location of the script (this is for running via symlink)
SCRIPT_PATH="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_PATH" ]; do
  # resolve $SCRIPT_PATH until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_PATH" )" >/dev/null && pwd )"
  SCRIPT_PATH="$(readlink "$SCRIPT_PATH")"
  # if $SCRIPT_DIR was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  [[ $SCRIPT_PATH != /* ]] && SCRIPT_DIR="$SCRIPT_DIR/$SCRIPT_PATH"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_PATH" )" >/dev/null && pwd )"

# import settings
source $SCRIPT_DIR/make_ink_config_template
source $SCRIPT_DIR/make_ink_config

MENU_ENTRIES_DIR="$HOME/.local/share/applications"

# Needed for version numbers and unique directories
DATE=$(date +%Y.%m.%d.%H.%M.%S)

MARKER="-->"

# Do we want to do a full rebuild? This also allows to build Inkscape if the branch has not changed.
FORCE=0

# --- FUNCTIONS ---

# Create menu entries
# arguments:
# $1 name
# $2 comment,
# $3 script to execute,
# $4 terminal: true or false
# $5 file name for desktop file (without file extension)
function create_menu_entry () {

echo "[Desktop Entry]
Type=Application
Name=$1
Comment=$2
Exec=$3
Icon=inkscape
Terminal=$4
Categories=Graphics;
"  > $MENU_ENTRIES_DIR/$5

xdg-desktop-menu install $MENU_ENTRIES_DIR/$5
}


# --- ACTUAL SCRIPT ---

# Get into correct directory
cd $SOURCE_DIR

# don't update everything just yet, to not mess up revision numbers for later comparison
git fetch

REMOTE=
GITLAB_USER=

# select correct branch
if [ $# -eq 0 ]
  then
    BRANCH="master"
elif [ $# -gt 0 ]
  then
    BRANCH=$1;
    if [ "${BRANCH:0:8}" == "https://" ]
      then
        if (grep -vq /inkscape/-/tree/ <<< "$BRANCH"); then
          echo "URL must be https://gitlab.com/<user>/inkscape/-/tree/<branch>."
          exit 1
        fi
        REMOTE="${BRANCH%%/-/tree/*}.git"
        BRANCH="${BRANCH#*/-/tree/}"
        GITLAB_USER="$(cut -d/ -f4 <<< "$REMOTE")"
        echo "$MARKER Remote $REMOTE selected."
    elif [ ! "`git branch --list -r origin/${BRANCH} `" ]
      then
        echo "Sorry, the selected branch $BRANCH doesn't exist in the remote repository. Please select a different branch."
        exit 1;
    fi
    if [ $# -eq 2 ]
      then
        if [ $2 = '-f' ]
          then
            FORCE=1
        fi
    fi
else
  echo "Too many arguments. This program takes either the name of a branch and an optional -f, or no argument to default to the master branch.";
  exit 1;
fi

echo "$MARKER Branch $BRANCH selected."

if [ -n "$REMOTE" ]; then
  echo "($GITLAB_USER)";
fi

# This is the safer way to do it, only doesn't work on Ubuntu 16.04 (but does from 18.04 up)
git submodule deinit --all -f

# this is what git suggests to get rid of the submodule in Ubuntu 16.04 (# with git 2.7.4...):
# rm -rf share/extensions

# cleanup, so there won't be anything to commit, causing git to refuse switching branches
git checkout -- .
git clean -d -f

# get into correct branch
if [ -z "$REMOTE" ]; then
  git checkout $BRANCH;

  # check if there are new commits
  REVISION_OLD=$(git log -1 --format="%h")
  git pull
else
  REVISION_OLD=none
  git fetch $REMOTE $BRANCH
  git checkout FETCH_HEAD
fi

git submodule update --init --recursive

REVISION=$(git log -1 --format="%h")

if [ -z "$REMOTE" ]; then
  BRANCH_SPECIFIER="$BRANCH"
  BRANCH_VERBOSE="$BRANCH"
else
  BRANCH_SPECIFIER=${GITLAB_USER}_$BRANCH
  BRANCH_VERBOSE="$BRANCH ($GITLAB_USER)"
fi

# Prepare file and package names:
# to lowercase (bash only)
BRANCH_SPECIFIER="${BRANCH_SPECIFIER,,}"
# replace underscores with spaces (bash only)
BRANCH_SPECIFIER="${BRANCH_SPECIFIER//_/-}"

INSTALL_DIR=$INSTALL_TO/inkscape_$BRANCH_SPECIFIER

if [ ! -f "$INSTALL_DIR/bin/inkscape" ] || [ ! -f "$BUILDS_DIR/build_$BRANCH_SPECIFIER/bin/inkscape" ] || [ ! "$REVISION" == "$REVISION_OLD" ] || [ $FORCE = 1 ]; then
    echo "$MARKER Update in progress..."
else
    echo "$MARKER No update needed! Your installation is already up-to-date."
    exit 0
fi

# Collect info about this version

if [ -z "$REMOTE" ]; then
  REMOTE=$(git remote get-url origin)
fi

# remove build directory if we want to force a full rebuild
if [ $FORCE = 1 ]
    then
        # Delete any previous builds, if they exist
        echo "$MARKER Removing old build directory (build_$BRANCH_SPECIFIER) ..."
        rm -rf $BUILDS_DIR/build_$BRANCH_SPECIFIER
fi

mkdir -p $BUILDS_DIR/build_$BRANCH_SPECIFIER
cd $BUILDS_DIR/build_$BRANCH_SPECIFIER

# Build
echo "$MARKER Configuring and compiling..."
cmake -DWITH_INTERNAL_2GEOM=ON -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR $SOURCE_DIR -G Ninja 
#cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR $SOURCE_DIR -G Ninja 
nice ninja

# Create description file
echo "Inkscape Development version as of $DATE, revision $REVISION, branch $BRANCH_VERBOSE)" > description-pak

# Create directory where to save the package
PACKAGE_DIR=$SAVE_DEB_TO/${BRANCH_SPECIFIER}_$DATE
mkdir -p $PACKAGE_DIR

# Build the .deb package
echo "$MARKER Building Debian package..."
checkinstall -y -D --fstrans=yes --install=no --pkgname inkscape-$BRANCH_SPECIFIER --pkgversion $DATE$REVISION --nodoc --pkglicense "GPL" --maintainer "$USER" --arch amd64 --pakdir $PACKAGE_DIR --pkgsource "$REMOTE" --exclude="$SOURCE_DIR/.git" ninja install

# Create user configuration directory, if doesn't exist
PROFILE_DIR=$SAVE_PROFILES_TO/inkscape_$BRANCH_SPECIFIER

mkdir -p $PROFILE_DIR

# Install new package, if sudo is available for the current user:
if groups | grep -qw "sudo"; then
    echo "$MARKER Installing new version..."
    notify-send 'Build Completed!' 'Please enter your password to install.' --icon=dialog-information
    sudo dpkg -i $PACKAGE_DIR/inkscape-*.deb
fi

INK_PROG="$INSTALL_DIR/bin/inkscape"
INK_DIR="$INSTALL_DIR/bin"
STARTER_FILE=$STARTER_FILE_PATH/Run_Ink_$BRANCH_SPECIFIER
STARTER_FILE_DEBUG=$STARTER_FILE_PATH/Db_Ink_$BRANCH_SPECIFIER

# create directory for menu entries if it doesn't exist yet
mkdir -p $MENU_ENTRIES_DIR

# 1. Create start script (no debug)
echo "#!/bin/bash

export INKSCAPE_PROFILE_DIR="$PROFILE_DIR";PATH=$INK_DIR:\$PATH;$INK_PROG --app-id-tag $BRANCH_SPECIFIER" > $STARTER_FILE

# 2. Create start script (debug)
echo "#!/bin/bash

# functions for gdb debugging
log() {
    echo \"\${*}\" 1>&2
}

die() {
    usage
    log 'error:' \${*}'.'
    exit 1
}

DATE=\$(date +%Y.%m.%d.%H.%M.%S)

export INKSCAPE_PROFILE_DIR="$PROFILE_DIR";PATH=$INK_DIR:\$PATH

LOG=\"/tmp/inkscape_debug_trace_\${DATE}.txt\"
log \"outputting trace to '\${LOG}'\"

exec gdb -batch-silent \\
    -ex 'set logging overwrite on' \\
    -ex \"set logging file \${LOG}\" \\
    -ex 'set logging on' \\
    -ex 'handle SIG33 pass nostop noprint' \\
    -ex 'set pagination 0' \\
    -ex 'run' \\
    -ex 'backtrace full' \\
    -ex 'info registers' \\
    -ex 'x/16i \$pc' \\
    -ex 'thread apply all backtrace' \\
    -ex 'quit' \\
    --args $INK_PROG \\
    < /dev/null

" > $STARTER_FILE_DEBUG

# Make start scripts executable
chmod +x $STARTER_FILE
chmod +x $STARTER_FILE_DEBUG

create_menu_entry "Inkscape $BRANCH_VERBOSE" "Inkscape Custom Build ($DATE, revision $REVISION, branch $BRANCH_VERBOSE)" $STARTER_FILE false inkscape-$BRANCH_SPECIFIER.desktop

create_menu_entry "Inkscape $BRANCH_VERBOSE (debug)" "Inkscape Custom Debug Build ($DATE, revision $REVISION, branch $BRANCH_VERBOSE)" $STARTER_FILE_DEBUG false inkscape-$BRANCH-debug.desktop

# Some distros do not have sudo:
if ! groups | grep -qw "sudo"; then
    echo "$MARKER Install your new Inkscape version with administrator permissions:
dpkg -i $PACKAGE_DIR/inkscape-*.deb"
    notify-send 'Build Completed!' 'Please install the new Inkscape package.' --icon=dialog-information
fi

echo "Start your Inkscape installation by executing

$STARTER_FILE (or $STARTER_FILE_DEBUG)

The .deb package that has been built is available in

$SAVE_DEB_TO"

notify-send 'Package installed!' "Enjoy running your new Inkscape build of $BRANCH_VERBOSE!" --icon=dialog-information
